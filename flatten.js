
module.exports.flatten = function(array){
    
    const  newArray = [];

    function flatten_helper(array){
        for(let i=0; i<array.length; i++){
            let element = array[i];
            if(Array.isArray(element)){
                flatten_helper(element);
            }else{
                newArray.push(element);
            }
        }
    }
    flatten_helper(array);
    return newArray;
}