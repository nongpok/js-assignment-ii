
module.exports.filter = function(items, cb){

    let newArray = [];
    let j=0;
    for(let i=0; i<items.length; i++){
        if(cb(items[i])){
            newArray[j] = items[i];
            j++;
        }
    }

    return newArray;
}