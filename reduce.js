module.exports.reduce = function(items, cb, startingValue){

    let reducedValue = 0;
    let i=0;
    if(startingValue == undefined){
        startingValue = items[0];
        i=1;
    }
    while(i<items.length){
        reducedValue = cb(startingValue, items[i]);
        startingValue = reducedValue;
        i++;
    }
    return reducedValue;
}