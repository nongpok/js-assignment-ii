
module.exports.map = function(items,cb){

    let newArray = [];
    for(let i=0; i<items.length; i++){
        let newElement = cb(items[i]);
        newArray[i] = newElement;
    }
    return newArray;
}