//required the module
const solution = require('../each')

//callback function
function cb(element, index){
    console.log("function cb executed for index " + index + " whose element is: " + element);
}



const items = [1, 2, 3, 4, 5, 5];

//tested the each function
solution.each(items, cb);