const solution = require('../find');

function cb(element){
    return element % 2 == 0;
}

const items = [1, 2, 3, 4, 5, 5];

const findValue = solution.find(items, cb);
console.log(findValue);