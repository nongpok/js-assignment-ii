const solution = require('../filter');

function cb(element){
    return element % 2 == 0;
}

const items = [1, 2, 3, 4, 5, 5];

const newArray = solution.filter(items, cb);
console.log(newArray);