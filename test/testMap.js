const solution = require('../map');

function cb(element){
    return element * 3;
}

const items = [1, 2, 3, 4, 5, 5];

const newArray = solution.map(items, cb);
console.log(newArray);