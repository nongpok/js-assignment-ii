const solution = require('../reduce');

function cb(startingValue, element){
    return startingValue + element;
}

const items = [1, 2, 3, 4, 5, 5];

let startingValue = 10;

let reducedValue = solution.reduce(items, cb, startingValue);
console.log(reducedValue);